angular.module('myApp', []).controller('namesCtrl', function($scope) {
    $scope.names = [
        {name:'Ece Ayhan',country:'Turkish'},
        {name:'Kerstin Ekman',country:'Sweden'},
        {name:'Jane Austen',country:'England'},
        {name:'Erlend Loe',country:'Norway'},
        {name:'Fredrik Bajer',country:'Denmark'},
        {name:'Hakan Nesser',country:'Sweden'},
        {name:'Ludvig Holberg',country:'Denmark'},
        {name:'Thomas Hardy',country:'England'},
        {name:'Linn Ullmann',country:'Norway'}
    ];
});